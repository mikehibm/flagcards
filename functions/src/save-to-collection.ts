/* eslint-disable @typescript-eslint/no-explicit-any */
import * as admin from 'firebase-admin';

export const saveToCollection = async (
  db: FirebaseFirestore.Firestore,
  parentDoc: admin.firestore.DocumentReference | null,
  collection: string,
  data: any,
) => {
  try {
    const collectionRef = parentDoc
      ? parentDoc.collection(collection)
      : db.collection(collection);
    const docRef = data.id
      ? collectionRef.doc(data.id)
      : await collectionRef.add({});

    const collectionKeys: string[] = Object.keys(data)
      .map(key => {
        if (key.startsWith('_')) {
          return key.substr(1);
        }

        return '';
      })
      .filter(k => !!k);
    // console.log('collectionKeys', collectionKeys);

    const collectionValues = collectionKeys.map(k => {
      return {
        key: k,
        value: data[`_${k}`],
      };
    });
    // console.log('collectionValues', collectionValues);

    const dataWithoutId = {
      ...data,
      createdAt: admin.firestore.FieldValue.serverTimestamp(),
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    };
    delete dataWithoutId.id;
    for (const k of collectionKeys) {
      delete dataWithoutId[`_${k}`];
    }
    // console.log('dataWithoutId', dataWithoutId);

    await docRef.set(dataWithoutId);

    for await (const obj of collectionValues) {
      // console.log('obj', obj);
      for await (const subData of obj.value) {
        await saveToCollection(db, docRef, obj.key, subData);
      }
    }
  } catch (error) {
    console.log(error);
  }
};
