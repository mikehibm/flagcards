import * as puppeteer from 'puppeteer';
import { WIKIPEDIA_URL } from './constants';
import flatten from './array-flatten';

let browser: puppeteer.Browser;
let page: puppeteer.Page;

async function getBrowserPage(): Promise<puppeteer.Page> {
  // const isDebug = process.env.NODE_ENV !== 'production';

  const launchOptions = {
    // headless: isDebug ? false : true,
    headless: true,
    args: ['--no-sandbox'],
  };

  browser = await puppeteer.launch(launchOptions);
  const page = await browser.newPage();
  page.setDefaultNavigationTimeout(0);
  return page;
}

export async function getFlagList() {
  const url = WIKIPEDIA_URL;
  //  if (!browser || !page) {
  page = await getBrowserPage();
  //  }

  await page.goto(url, { waitUntil: 'domcontentloaded' }); // domを読み込むまで待機

  // タブを移動
  const pages = await browser.pages();
  const detailPage = pages[1];
  await detailPage.bringToFront();

  // // タイトル取得
  // const title = await detailPage.titile();
  // console.log(title);

  // 国旗一覧取得
  const content = await detailPage.$$eval(
    '#mw-content-text > div > table',
    elements => {
      // 1行目に「国名」を含むテーブルだけを取得。
      const tables = elements.filter(e =>
        e.querySelector('tr > th')?.textContent?.includes('国名'),
      );

      return tables.map(e =>
        Array.from(e.querySelectorAll('tr')).map(tr => {
          // 国名が太字（<b>国名</b>）で記載されているもののみに絞る。
          if (!tr.querySelector('td:nth-child(1) > b > a')) return undefined;

          const name = tr.querySelector('td:nth-child(1) > b > a')?.innerHTML;
          const country_url = tr
            .querySelector('td:nth-child(1) > b > a')
            ?.getAttribute('href');
          const flag_ratio = tr.querySelector('td:nth-child(2)')?.innerHTML;
          let flag_url =
            tr
              .querySelector('td:nth-child(3) > a > img')
              ?.getAttribute('src')
              ?.replace('/thumb/', '/')
              ?.split('/')
              ?.slice(0, -1)
              ?.join('/') || '';
          flag_url = flag_url ? 'https:' + flag_url : '';
          const flag_file = flag_url.split('/').pop() || '';
          const name_eng =
            flag_file?.replace('Flag_of_', '')?.replace('.svg', '') || '';

          return {
            name,
            name_eng,
            flag_ratio,
            flag_url,
            flag_file,
            country_url,
          };
        }),
      );
    },
  );

  detailPage.removeAllListeners();
  detailPage.close();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const countries = flatten(content as Array<Array<any>>, 1)
    .filter(i => !!i)
    .map(i => {
      return {
        ...i,
        country_url: `https://ja.wikipedia.org${i.country_url}`,
      };
    });
  // console.log(JSON.stringify(countries, null, 2));
  console.log('COUNTRIES: ', countries.length);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  async function getInfo(c: any) {
    console.log(c.name, c.country_url);

    const countryPage = await browser.newPage();
    countryPage.setDefaultNavigationTimeout(0);

    await countryPage.goto(c.country_url, {
      waitUntil: 'domcontentloaded',
    }); // domを読み込むまで待機

    const info = await countryPage.$$eval(
      '#infoboxCountry .infoboxCountryDataB > table',
      tables => {
        const table = tables[0];
        if (!table) return;
        const rows = Array.from(table.querySelectorAll('tr'));

        // 首都
        const capital =
          rows
            .map(r => r.querySelector('th > a[title=首都]'))
            .filter(a => !!a)[0]
            ?.closest('tr')
            ?.querySelector('td > a')
            ?.getAttribute('title') || '';

        // 人口
        const populationStr =
          table
            .querySelector('.infoboxCountrySome a[title=人口]')
            ?.parentElement?.nextElementSibling?.querySelector('tr > td > a')
            ?.textContent?.replace(/,/g, '') || '';
        let population = parseInt(populationStr, 10);
        if (populationStr.includes('万')) population *= 10000;

        return { capital, population, populationStr };
      },
    );

    await countryPage.removeAllListeners();
    await countryPage.close();

    console.log(
      `${c.name}: '${info?.capital}', ${info?.population}, ${info?.populationStr}人`,
    );

    return {
      ...c,
      capital: info?.capital,
      population: info?.population,
      populationStr: info?.populationStr,
    };
  }

  const promises = countries.map(async c => await getInfo(c));
  const list = await Promise.all(promises);

  return { result: 'success', countries: list };
}
