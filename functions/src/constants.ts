export const WIKIPEDIA_URL =
  'https://ja.wikipedia.org/wiki/%E5%9B%BD%E6%97%97%E3%81%AE%E4%B8%80%E8%A6%A7';

export const COLLECTIONS = {
  countries: 'countries',
  docCounters: 'docCounters',
};
