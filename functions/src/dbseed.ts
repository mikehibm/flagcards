import * as commander from 'commander';
import * as admin from 'firebase-admin';
import * as fs from 'fs';
import { COLLECTIONS } from './constants';
import { saveToCollection } from './save-to-collection';
import * as serviceAccount from './flagcards-firebase-adminsdk.json';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as admin.ServiceAccount),
  databaseURL: 'https://flagcards.firebaseio.com',
});

const db = admin.firestore();

type Country = {
  name: string;
  name_eng: string;
  ratio: string;
  flag_url: string;
  flag_name: string;
};

const uploadSeed = async (collection: string, seedFile: string) => {
  const buffer = fs.readFileSync(seedFile);
  const data = JSON.parse(buffer.toString());

  switch (collection) {
    case COLLECTIONS.countries: {
      const countries = (data[collection] as Array<Country>).map(i => {
        return {
          ...i,
          id: undefined,
        };
      });

      for await (const country of countries) {
        await saveToCollection(db, null, COLLECTIONS.countries, country);
      }

      return;
    }

    default: {
      throw new Error('Specify target collection');
    }
  }
};

commander
  .version('0.1.0', '-v, --version')
  .arguments('<collection> <seedFile>')
  .action(uploadSeed);

commander.parse(process.argv);
