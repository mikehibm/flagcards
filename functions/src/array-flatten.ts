// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default function flatten(arr: any[], depth: any) {
  const flattend = [];
  (function flat(array, depth) {
    for (const el of array) {
      if (Array.isArray(el) && depth > 0) {
        flat(el, depth - 1);
      } else {
        flattend.push(el);
      }
    }
  })(arr, Math.floor(depth) || 1);
  return flattend;
}
