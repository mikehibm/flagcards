import * as puppeteer from 'puppeteer';

let browser: puppeteer.Browser;
let page: puppeteer.Page;

async function getBrowserPage(): Promise<puppeteer.Page> {
  // const isDebug = process.env.NODE_ENV !== 'production';

  const launchOptions = {
    // headless: isDebug ? false : true,
    headless: true,
    defaultViewport: null,
    args: ['--no-sandbox'],
  };

  browser = await puppeteer.launch(launchOptions);
  return browser.newPage();
}

export async function getScreenshotImage(url: string) {
  if (!browser || !page) {
    page = await getBrowserPage();
  }

  await page.setViewport({
    width: 960,
    height: 1200,
    deviceScaleFactor: 960 / 400, // Default=1.0
    isMobile: true,
    hasTouch: true,
    isLandscape: false,
  });

  await page.goto(url, { waitUntil: 'domcontentloaded' }); // domを読み込むまで待機
  const imageBuffer = await page.screenshot();
  return imageBuffer;
}
