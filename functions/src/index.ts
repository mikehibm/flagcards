/* eslint-disable @typescript-eslint/no-explicit-any */
import * as functions from 'firebase-functions';
// import { WIKIPEDIA_URL } from './constants';
// import { getScreenshotImage } from './puppet-get-screenshot';
import { getFlagList } from './puppet-get-flags';

// exports.screenshot = functions
//   .runWith({ timeoutSeconds: 60, memory: '1GB' })
//   .https.onRequest(async (req, res) => {
//     const url = WIKIPEDIA_URL;
//     const imageBuffer = await getScreenshotImage(url);
//     res.set('Content-Type', 'image/png');
//     res.send(imageBuffer);
//   });

exports.flags = functions
  .runWith({ timeoutSeconds: 300, memory: '1GB' })
  .https.onRequest(async (req, res) => {
    const data = await getFlagList();
    res.set('Content-Type', 'application/json');
    res.json(data);
  });
