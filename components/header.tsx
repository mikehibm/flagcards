import React from 'react';

type HeaderProps = {
  isServer: boolean;
  isProduction: boolean;
};

export const Header = ({ isServer, isProduction }: HeaderProps) => {
  return (
    <header>
      <div className="app-logo">
        <img src="/img/logo.png" alt="logo" />
        <h1 className="app-title">Flags</h1>
      </div>
      <p className="app-subtitle">世界の国旗をおぼえよう！</p>

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        header {
          display: grid;
          grid-template-columns: min-content 1fr;
          column-gap: 1rem;

          padding: 0.1rem;
          color: #444;
          background: #eee;
          text-align: center;

          color: #e44;
          background-color: '#eee';
        }

        header .app-logo {
          display: grid;
          grid-template-columns: min-content min-content;
          padding-left: 1rem;
        }

        header .app-logo > img {
          height: 72px;
        }

        header .app-title {
          margin: 1rem 0.2rem;
          font-size: 1.6rem;
          color: #448;
        }

        header .app-subtitle {
          margin-top: 1.4rem;
        }

        /* 縦長画面の場合 */
        @media (max-aspect-ratio: 10/10) {
          header {
            display: block;
            padding: 0.6rem;
          }

          header .app-logo {
            margin-left: 10vw;
          }
          header .app-title {
            margin: 1rem 0;
          }
          header .app-subtitle {
            margin: 0 0;
          }
        }
      `}</style>
    </header>
  );
};
