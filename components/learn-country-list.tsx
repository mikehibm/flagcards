import React, { ReactEventHandler } from 'react';
import { sendGAEvent } from '../utils/send-ga-event';
import { Country } from '../state/country';
import { Stage } from '../state/stage';
import { Card } from './card';
import { STAGE_COUNT } from '../utils/constants';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faRedoAlt,
  faArrowUp,
  faArrowRight,
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons';

type Props = {
  stageIndex: number;
  stage: Stage;
  onBackToStages: ReactEventHandler;
  onNextStage: (newIndex: number) => void;
  onFlipCard: (c: Country) => void;
  onCheckCard: (c: Country) => void;
  isTouchEnabled: boolean;
};

export const CountryList: React.FC<Props> = ({
  stageIndex,
  stage,
  onBackToStages,
  onNextStage,
  onFlipCard,
  onCheckCard,
  isTouchEnabled,
}) => {
  const refresh = (ev: React.MouseEvent) => {
    ev.preventDefault();
    sendGAEvent('learn_button_refresh', { stageIndex: stageIndex });
    onNextStage(stageIndex);
  };

  const handlePrev = (ev: React.MouseEvent) => {
    ev.preventDefault();
    sendGAEvent('learn_button_prev', { stageIndex: stageIndex - 1 });
    onNextStage(stageIndex - 1);
  };

  const handleNext = (ev: React.MouseEvent) => {
    ev.preventDefault();
    sendGAEvent('learn_button_next', { stageIndex: stageIndex + 1 });
    onNextStage(stageIndex + 1);
  };

  // console.log('Re-rendering CountryList. stageIndex=', stageIndex);

  return (
    <div>
      <div className="flag-list">
        {stage.countries.map(i => (
          <Card
            key={i.name_eng}
            country={i}
            isTouchEnabled={isTouchEnabled}
            onClick={onFlipCard}
            onCheckDone={onCheckCard}
          />
        ))}
      </div>

      <div className="buttons">
        <button className="btn-stages" type="button" onClick={onBackToStages}>
          <FontAwesomeIcon icon={faArrowUp} />
        </button>
        {stageIndex > 0 && (
          <button className="btn-prev" type="button" onClick={handlePrev}>
            <FontAwesomeIcon icon={faArrowLeft} />
          </button>
        )}
        <button className="btn-refresh" type="button" onClick={refresh}>
          <FontAwesomeIcon icon={faRedoAlt} />
        </button>
        {stageIndex < STAGE_COUNT - 1 && (
          <button className="btn-next" type="button" onClick={handleNext}>
            <FontAwesomeIcon icon={faArrowRight} />
          </button>
        )}
      </div>

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        .buttons {
          margin: 1.6rem 1.2rem 0;
          text-align: center;
          display: grid;
          grid-auto-columns: auto;
          grid-auto-flow: column;
          justify-content: center;
          column-gap: 2vw;
        }
        .buttons button {
          padding: 0.8rem 1.4rem;
          font-size: 1.4rem;
          color: blue;
          background-color: #cef;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }
        .buttons button[disabled] {
          color: gray;
          background-color: #ccc;
          cursor: default;
        }

        .flag-list {
          margin-top: 1rem;
          display: grid;
          grid-template-columns: repeat(auto-fit, minmax(112px, 1fr));
          grid-auto-rows: minmax(126px, 1fr);
          column-gap: 1rem;
          row-gap: 1rem;
          justify-content: center;
          align-content: center;
        }

        @media (max-aspect-ratio: 10/10) {
          .buttons {
            margin-top: 1.6rem;
            column-gap: 0.8rem;
          }
        }
      `}</style>
    </div>
  );
};
