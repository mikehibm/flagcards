import React, { useEffect } from 'react';
import { Stage } from '../state/stage';

type Props = {
  stage: Stage;
  questionIndex: number;
  onNextQuestion: () => void;
  onSelectAnswer: (answerIndex: number) => void;
};

export const QuizNameToFlag: React.FC<Props> = ({
  stage,
  questionIndex,
  onSelectAnswer,
}) => {
  useEffect(() => {
    const btn = document.querySelector('.answers .btn') as HTMLButtonElement;
    console.log('First answers button', btn);
    if (btn) {
      btn.focus();
      btn.blur();
    }
  }, []);

  const { name, answers } = stage.countries[questionIndex];

  const handleSelectAnswer = (e: React.MouseEvent) => {
    e.preventDefault();

    const answerIndex = parseInt(
      e.currentTarget.getAttribute('data-index') || '',
      10,
    );
    onSelectAnswer(answerIndex);
  };

  return (
    <div className="nameToFlag">
      <div className="question">
        <div className="name">{name}</div>
      </div>
      <div className="answers">
        {answers.map((a, ix) => (
          <button
            key={a.name_eng}
            className="btn"
            onClick={handleSelectAnswer}
            data-index={ix}
          >
            <img src={`/img/flags/${a.flag_file}`} alt={`Answer ${ix + 1}`} />
          </button>
        ))}
      </div>
      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        .question {
          display: grid;
          grid-template-columns: 1fr;
          box-sizing: content-box;
        }

        .name {
          margin: 1rem;
          padding: 0.6rem;
          font-size: 1.4rem;
          color: blue;
          background-color: rgba(255, 220, 220, 0.8);
          border-radius: 0.8rem;
          box-shadow: 0 0.6rem 1rem rgba(0, 0, 0, 0.15);
        }
        .answers {
          text-align: center;
        }

        .answers .btn {
          display: inline-block;
          margin: 0.6rem 0.4rem;
          padding: 0.6rem 0.8rem;
          background-color: aquamarine;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }
        .answers .btn img {
          height: 14vmin;
        }

        .answers .btn:hover {
          color: white;
          background-color: blue;
        }
      `}</style>
    </div>
  );
};
