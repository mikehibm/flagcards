import React from 'react';
import Link from 'next/link';

type FooterProps = {
  isServer: boolean;
  isProduction: boolean;
};

export const Footer = ({ isServer, isProduction }: FooterProps) => {
  return (
    <footer>
      {!isProduction && (
        <>
          &copy;FlagCards {isServer ? 'Server' : 'Client'}{' '}
          <Link href="/">
            <a>Home</a>
          </Link>{' '}
          |{' '}
          <Link href="/about">
            <a>About</a>
          </Link>
        </>
      )}

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        footer {
          margin-top: 2rem;
          margin-bottom: 2rem;
          text-align: center;
          color: #444;
          font-size: 0.6rem;
          opacity: 0.6;
        }

        @media (max-aspect-ratio: 10/10) {
          footer {
            margin-top: 4rem;
          }
        }
      `}</style>
    </footer>
  );
};
