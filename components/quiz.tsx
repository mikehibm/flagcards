import React from 'react';
import { Stage } from '../state/stage';
import { QuizFlagToName } from './quiz-flag-to-name';
import { QuizNameToFlag } from './quiz-name-to-flag';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowUp,
  faArrowRight,
  faThumbsUp,
  faMehRollingEyes,
  faTimes,
} from '@fortawesome/free-solid-svg-icons';

type Props = {
  stage: Stage;
  questionIndex: number;
  questionResult: 'OK' | 'NG' | null;
  isQuestionResultHidden: boolean;
  onNextQuestion: () => void;
  onSelectAnswer: (answerIndex: number) => void;
  onBackToStages: () => void;
};

export const Quiz: React.FC<Props> = ({
  stage,
  questionIndex,
  questionResult,
  isQuestionResultHidden,
  onNextQuestion,
  onSelectAnswer,
  onBackToStages,
}) => {
  const question = stage.countries[questionIndex];

  console.log('Re-rendering Quiz', isQuestionResultHidden);

  return (
    <div className="quiz">
      <div className="stage">
        <button className="btn-back" onClick={onBackToStages}>
          <FontAwesomeIcon icon={faArrowUp} />
        </button>
        Stage {stage.stageIndex + 1}{' '}
        <span className="question-header">
          {questionIndex + 1}/{stage.countries.length}
          <button className="btn-skip" onClick={onNextQuestion}>
            <FontAwesomeIcon icon={faArrowRight} />
          </button>
        </span>
      </div>
      <div className="question">
        {question.gameMode === 'flagToName' && (
          <QuizFlagToName
            key={question.name_eng}
            stage={stage}
            questionIndex={questionIndex}
            onNextQuestion={onNextQuestion}
            onSelectAnswer={onSelectAnswer}
          />
        )}
        {question.gameMode === 'nameToFlag' && (
          <QuizNameToFlag
            key={question.name_eng}
            stage={stage}
            questionIndex={questionIndex}
            onNextQuestion={onNextQuestion}
            onSelectAnswer={onSelectAnswer}
          />
        )}

        {questionResult === 'OK' && (
          <div className="result-ok">
            <FontAwesomeIcon icon={faThumbsUp} />
          </div>
        )}
        {questionResult === 'NG' && (
          <div className={`result-ng ${isQuestionResultHidden && 'hidden'}`}>
            <FontAwesomeIcon icon={faMehRollingEyes} spin />
          </div>
        )}
      </div>

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        .quiz {
          position: relative;
        }

        .stage {
          font-size: 1.2rem;
          color: blue;
        }

        .question {
          margin-top: 1rem;
          text-align: center;
        }
        .question-header {
          text-align: left;
          color: darkred;
        }

        .stage button {
          padding: 0.4rem 0.6rem 0.4rem 0.6em;
          text-align: center;
          vertical-align: middle;
          font-size: 1rem;
          color: #666;
          background-color: #ccf;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }

        .stage button.btn-skip {
          position: absolute;
          right: 0;
        }
        .stage button.btn-back {
          margin-right: 0.8rem;
        }

        .result-ok,
        .result-ng {
          position: absolute;
          display: flex;
          justify-content: center;
          align-items: center;
          width: 45vmin;
          height: 46vmin;
          top: 0;
          bottom: 0;
          left: 0;
          right: 0;
          margin: auto;
          text-align: center;
          vertical-align: middle;
          font-size: 6rem;
          color: white;
          border-radius: 4vmin;
          box-shadow: 0 0.6rem 1rem rgba(0, 0, 0, 0.15);
          transition: 0.2s ease-in;
          opacity: 1;
        }
        .result-ok {
          background-color: rgba(41, 160, 20, 1);
          border: 2vmin solid #262;
          border-radius: 50%;
        }

        .result-ng {
          background-color: orange;
          border: 2vmin solid red;
        }
        .result-ng.hidden {
          opacity: 0;
          width: 0;
          height: 0;
        }
      `}</style>
    </div>
  );
};
