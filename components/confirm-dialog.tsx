import React, { useState, useCallback, useEffect } from 'react';

type Props = {
  message: string;
  onOkCallback?: () => void;
  onNgCallback?: () => void;
  className?: string;
};

const ANIMATION_TIME = 0.6;

export function ConfirmDialog({
  message,
  className = '',
  onOkCallback,
  onNgCallback,
}: Props) {
  const [isRendered, setIsRendered] = useState(true);
  const [isHidden, setIsHidden] = useState(true);
  useEffect(() => {
    setIsHidden(false);
  }, [isRendered]);

  const handleClose = useCallback(async (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    setIsHidden(true);
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(setIsRendered(false));
      }, ANIMATION_TIME * 1000);
    });
  }, []);

  const handleOkClick = useCallback(
    async (e: React.MouseEvent<HTMLElement>) => {
      await handleClose(e);
      onOkCallback && onOkCallback();
    },
    [onOkCallback],
  );

  const handleNgClick = useCallback(
    async (e: React.MouseEvent<HTMLElement>) => {
      await handleClose(e);
      onNgCallback && onNgCallback();
    },
    [onNgCallback],
  );

  const handleClickInside = useCallback((e: React.MouseEvent<HTMLElement>) => {
    console.log('handleClickInside');
    e.preventDefault();
    e.stopPropagation();
  }, []);

  if (!isRendered) return null;

  return (
    <div
      className="dialog-wrapper"
      style={{ opacity: isHidden ? '0' : '1' }}
      onClick={handleNgClick}
    >
      <div
        className={`dialog ${className}`}
        style={{ opacity: isHidden ? '0' : '1' }}
        onClick={handleClickInside}
      >
        <p className="message">{message}</p>
        <div className="buttons">
          <button className="btn-ok" onClick={handleOkClick}>
            YES
          </button>
          <button className="btn-ng" onClick={handleNgClick}>
            NO
          </button>
        </div>
      </div>

      <style jsx>{`
        .dialog-wrapper {
          position: absolute;
          width: 100vw;
          height: 100vh;
          top: 0;
          left: 0;
          background-color: rgba(0, 0, 0, 0.5);
          z-index: 20000;

          transition-property: opacity;
          transition-duration: ${ANIMATION_TIME}s;
        }

        .dialog {
          position: absolute;
          width: 80vw;
          height: 12vh;
          min-height: 12rem;
          bottom: 5vh;
          left: 10vw;
          display: grid;
          grid-template-rows: auto 1fr;
          align-items: center;
          justify-items: center;
          margin: 0;
          padding: 1rem 1rem 1rem 1rem;
          color: rgb(36, 35, 136);
          background-color: rgb(236, 233, 233);
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          text-align: center;
          z-index: 20100;

          transition-property: opacity;
          transition-duration: ${ANIMATION_TIME}s;
        }

        .message {
          width: 100%;
          margin: 0;
          padding: 0.9rem 0;
          font-size: 1rem;
        }

        .buttons {
          display: grid;
          width: 80%;
          max-width: 20rem;
          height: 80%;
          margin: 0 auto;
          padding: 0;
          top: 0;
          grid-template-columns: repeat(2, 1fr);
          column-gap: 2rem;
          text-align: center;
        }
        .buttons > button {
          width: 100%;
          height: max-content;
          padding: 0.6rem 0;
          font-size: 1rem;
          color: #00d;
          background-color: rgb(239, 244, 253);
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }
      `}</style>
    </div>
  );
}
