import React from 'react';
import { Stage } from '../state/stage';
import { Country } from '../state/country';
import { Card } from './card';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faArrowUp,
  faArrowLeft,
  faArrowRight,
} from '@fortawesome/free-solid-svg-icons';

type Props = {
  stage: Stage;
  cardIndex: number;
  onPrevCard: () => void;
  onNextCard: () => void;
  onFlipCard: (c: Country) => void;
  onCheckCard: (c: Country) => void;
  onBackToStages: () => void;
};

export const CountryCard: React.FC<Props> = ({
  stage,
  cardIndex,
  onPrevCard,
  onNextCard,
  onFlipCard,
  onCheckCard,
  onBackToStages,
}) => {
  const country = stage.countries[cardIndex];

  return (
    <div className="quiz">
      <div className="stage">
        <button className="btn-back" onClick={onBackToStages}>
          <FontAwesomeIcon icon={faArrowUp} />
        </button>
        <span className="stage-index">Stage {stage.stageIndex + 1}</span>
        <span className="question-header">
          {cardIndex + 1}/{stage.countries.length}
        </span>
      </div>
      <div className="card-wrapper">
        <Card
          country={country}
          isTouchEnabled={false}
          onClick={onFlipCard}
          onCheckDone={onCheckCard}
        />
      </div>
      <div className="question-footer">
        <button
          className="btn-prev"
          onClick={onPrevCard}
          disabled={cardIndex <= 0}
        >
          <FontAwesomeIcon icon={faArrowLeft} />
        </button>
        <button
          className="btn-skip"
          onClick={onNextCard}
          disabled={cardIndex >= stage.countries.length - 1}
        >
          <FontAwesomeIcon icon={faArrowRight} />
        </button>
      </div>

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        .quiz {
          position: relative;
        }

        .stage {
          font-size: 1.2rem;
          color: blue;
        }

        .stage-index,
        .question-header {
          display: inline-block;
        }
        .question-header {
          margin-left: 2rem;
          text-align: left;
          color: darkred;
        }

        .card-wrapper {
          width: 66vmin;
          height: 50vmin;
          margin: 0 auto;
          display: grid;
          grid-template-columns: 1fr;
          box-sizing: content-box;

          margin-top: 1rem;
          text-align: center;
        }

        .question-footer {
          display: grid;
          margin-top: 1.8rem;
          padding: 0 1rem;
          grid-template-columns: 1fr 1fr;
          column-gap: 1rem;
          justify-items: center;
          text-align: center;
        }

        .quiz button {
          padding: 0.4rem 0.6rem 0.4rem 0.6em;
          text-align: center;
          vertical-align: middle;
          font-size: 1rem;
          color: #666;
          background-color: #ddf;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }

        .quiz .question-footer button {
          width: 100%;
          max-width: 10rem;
        }

        .quiz .question-footer .btn-prev {
          justify-self: right;
        }
        .quiz .question-footer .btn-skip {
          justify-self: left;
        }

        .quiz .question-footer button[disabled] {
          background-color: #88c;
          cursor: default;
        }

        .quiz button.btn-back {
          margin-right: 0.8rem;
        }
      `}</style>
    </div>
  );
};
