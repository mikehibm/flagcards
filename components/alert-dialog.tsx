import React, { useState, useCallback, useEffect } from 'react';

type Props = {
  message: string;
  className?: string;
};

const ANIMATION_TIME = 0.8;

export function AlertDialog({ message, className = '' }: Props) {
  const [isRendered, setIsRendered] = useState(true);
  const [isHidden, setIsHidden] = useState(true);
  useEffect(() => {
    setIsHidden(false);
  }, []);

  const handleClickClose = useCallback((e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    setIsHidden(true);
    setTimeout(() => {
      setIsRendered(false);
    }, ANIMATION_TIME * 1000);
  }, []);

  const handleClickInside = useCallback((e: React.MouseEvent<HTMLElement>) => {
    console.log('handleClickInside');
    e.preventDefault();
    e.stopPropagation();
  }, []);

  if (!isRendered) return null;

  return (
    <div
      className="dialog-wrapper"
      style={{ opacity: isHidden ? '0' : '1' }}
      onClick={handleClickClose}
    >
      <div
        className={`dialog ${className}`}
        style={{ opacity: isHidden ? '0' : '1' }}
        onClick={handleClickInside}
      >
        <p className="message">{message}</p>
      </div>

      <style jsx>{`
        .dialog-wrapper {
          position: absolute;
          width: 100vw;
          height: 100vh;
          top: 0;
          left: 0;
          background-color: rgba(0, 0, 0, 0.5);
          z-index: 20000;

          transition-property: opacity;
          transition-duration: ${ANIMATION_TIME}s;
        }

        .dialog {
          position: absolute;
          width: 80vw;
          height: 10vh;
          min-height: 8rem;
          bottom: 5vh;
          left: 10vw;
          display: grid;
          grid-template-columns: 1fr;
          align-items: center;
          justify-items: center;
          margin: 0;
          padding: 0 1rem;
          color: rgb(36, 35, 136);
          background-color: rgb(236, 233, 233);
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          text-align: center;
          z-index: 20100;

          transition-property: opacity;
          transition-duration: ${ANIMATION_TIME}s;
        }
        .message {
          margin: 0;
          padding: 0;
        }
      `}</style>
    </div>
  );
}
