import React, { ReactEventHandler } from 'react';
import { Stage, getStageProgress } from '../state/stage';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faCheck } from '@fortawesome/free-solid-svg-icons';

type Props = {
  mode: 'learn' | 'game';
  stages: Stage[];
  isTouchEnabled: boolean;
  onClickStage: ReactEventHandler;
};

export const StageList: React.FC<Props> = ({ mode, stages, onClickStage }) => {
  return (
    <div>
      <div className="list">
        {stages.map((stage, i) => {
          const progress = getStageProgress(stage, mode);
          return (
            // eslint-disable-next-line react/no-array-index-key
            <div key={i} className="button-stage">
              <button
                onClick={onClickStage}
                data-index={i}
                style={{
                  backgroundColor: `hsl(236, 85%, ${66 +
                    32 * (1 - progress)}%)`,
                }}
              >
                {i + 1}
                <div
                  className="progress"
                  style={{ width: `${100 * progress}%` }}
                ></div>
              </button>
              {stage.allDone && (
                <div className="check">
                  <FontAwesomeIcon icon={faCheck} />
                </div>
              )}
            </div>
          );
        })}
      </div>

      <div className="buttons">
        <Link href="/">
          <button className="btn-home" type="button">
            <FontAwesomeIcon icon={faArrowLeft} />
          </button>
        </Link>
      </div>

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        .list {
          margin-top: 1rem;
          display: grid;
          grid-template-columns: repeat(auto-fit, minmax(60px, 1fr));
          grid-auto-rows: minmax(60px, 1fr);
          column-gap: 1rem;
          row-gap: 1rem;
          justify-content: center;
          align-content: center;
          text-align: center;
        }

        .list .button-stage {
          position: relative;
        }

        .list button {
          position: relative;
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
          font-size: 1.6rem;
          text-align: center;
          color: #00d;
          /* background-color: #cef; */
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }

        .list .check {
          position: absolute;
          top: -0.6rem;
          right: -0.2rem;
          font-size: 2rem;
          color: gold;
        }

        .buttons {
          margin-top: 1rem;
          text-align: center;
          display: grid;
          grid-auto-columns: auto;
          grid-auto-flow: column;
          justify-content: flex-start;
          column-gap: 1.2rem;
        }
        .buttons > button {
          padding: 0.6rem 1.4rem;
          font-size: 1.5rem;
          color: #00d;
          background-color: #cdf;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }

        .progress {
          position: absolute;
          bottom: 10%;
          width: 0%;
          height: 8%;
          background-color: gold;
        }

        @media (max-aspect-ratio: 10/10) {
          .list {
            margin-top: 1.6rem;
          }
        }
      `}</style>
    </div>
  );
};
