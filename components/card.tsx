import React, { useState, useCallback, useEffect, useRef } from 'react';
import { Country } from '../state/country';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle as faCheckCircleSolid } from '@fortawesome/free-solid-svg-icons';
import { faCheckCircle } from '@fortawesome/free-regular-svg-icons';

const FLIP_SPEED = 0.8;

type CardProps = {
  country: Country;
  isTouchEnabled: boolean;
  onClick: (c: Country) => void;
  onCheckDone: (c: Country) => void;
};

export const Card = ({
  country,
  isTouchEnabled,
  onClick,
  onCheckDone,
}: CardProps) => {
  const { name, flipped, flag_file, learn_count } = country;
  const [touched, setTouched] = useState(false);

  const spanRef = useRef<HTMLSpanElement>(null);

  useEffect(() => {
    if (!spanRef) return;
    const el = spanRef.current;
    if (!el) return;
    el.classList.add('hidden');
    const handle = setTimeout(() => {
      el.classList.remove('hidden');
    }, FLIP_SPEED * 1000);
    return () => clearTimeout(handle);
  }, [country.name]);

  const flipByClick = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (e: React.MouseEvent) => {
      e.preventDefault();
      e.stopPropagation();
      if (touched) return;
      onClick(country);
      setTouched(false);
    },
    [flipped, touched, country],
  );

  const flipByTouch = useCallback(() => {
    console.log('TouchStarted');
    // onClick(country);
    // setTouched(true);
  }, [flipped, country]);

  const onDblClick = useCallback(
    (e: React.MouseEvent) => e.preventDefault(),
    [],
  );

  const handleCheckDone = useCallback(
    (e: React.MouseEvent) => {
      e.preventDefault();
      e.stopPropagation();
      e.nativeEvent.stopImmediatePropagation();
      console.log('handleCheckDone', country.name_eng);
      onCheckDone(country);
      return false;
    },
    [learn_count, country],
  );

  const cardClass = `card ${flipped && 'flipped'}`;
  return (
    <div
      className="card-container"
      onClick={flipByClick}
      onTouchStart={isTouchEnabled ? flipByTouch : undefined}
      onDoubleClick={onDblClick}
    >
      <div className={cardClass}>
        <div className="front">
          <img src={`/img/flags/${flag_file}`} alt={name} width="78%" />
        </div>
        <div className="back">
          <span className="name" ref={spanRef}>
            {name}
          </span>
        </div>
      </div>
      <div
        className={`check ${learn_count > 0 && 'done'}`}
        onClick={handleCheckDone}
      >
        {learn_count > 0 ? (
          <FontAwesomeIcon icon={faCheckCircleSolid} />
        ) : (
          <FontAwesomeIcon icon={faCheckCircle} />
        )}
      </div>

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        .card-container {
          position: relative;
          -webkit-perspective: 800px;
          -ms-perspective: 800px;
          perspective: 800px;
          border-radius: 1.4vw;
        }
        .card {
          width: 100%;
          height: 100%;
          position: absolute;
          -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
          transition: transform cubic-bezier(0.175, 0.885, 0.32, 1.275)
            ${FLIP_SPEED}s;
          border-radius: 1.4vw;
          box-shadow: 0 1.6vw 3.2vw rgba(0, 0, 0, 0.15);
          cursor: pointer;
        }
        .card .front,
        .card .back {
          position: absolute;
          width: 100%;
          height: 100%;
          -webkit-backface-visibility: hidden;
          backface-visibility: hidden;
          border-radius: 1.4vw;
          background: #ccc;
          display: -ms-flexbox;
          display: box;
          display: flex;
          -o-box-pack: center;
          justify-content: center;
          -o-box-align: center;
          align-items: center;
          color: #47525d;
        }
        .card .front img {
          max-height: 80%;
        }
        .card-container .check {
          position: absolute;
          top: -0.5rem;
          right: -0.4rem;
          font-size: 2.6rem;
          color: #aaa;
          z-index: 1000;
          cursor: pointer;
        }
        .card-container .check.done {
          color: green;
        }

        .card .back {
          -webkit-transform: rotateY(180deg);
          transform: rotateY(180deg);
          background: #fff;
        }
        .card .back .name {
          display: inline-block;
          box-sizing: border-box;
          padding: 0.4rem;
          background: #fff;
        }
        .card .back .name.hidden {
          opacity: 0;
        }

        .card.flipped {
          -webkit-transform: rotateY(180deg);
          transform: rotateY(180deg);
        }
        .card .front :hover {
          background: #eee;
        }
      `}</style>
    </div>
  );
};
