import React, { useEffect } from 'react';
import { Stage } from '../state/stage';

type Props = {
  stage: Stage;
  questionIndex: number;
  onNextQuestion: () => void;
  onSelectAnswer: (answerIndex: number) => void;
};

export const QuizFlagToName: React.FC<Props> = ({
  stage,
  questionIndex,
  onSelectAnswer,
}) => {
  useEffect(() => {
    const btn = document.querySelector('.answers .btn') as HTMLButtonElement;
    console.log('First answers button', btn);
    if (btn) {
      btn.focus();
      btn.blur();
    }
  }, []);

  const { flag_file, name, answers } = stage.countries[questionIndex];

  const handleSelectAnswer = (e: React.MouseEvent) => {
    e.preventDefault();
    // e.currentTarget.blur();

    const answerIndex = parseInt(
      e.currentTarget.getAttribute('data-index') || '',
      10,
    );
    onSelectAnswer(answerIndex);
  };

  return (
    <div className="flagToName">
      <div className="question">
        <img
          className="question-flag"
          src={`/img/flags/${flag_file}`}
          alt={name}
        />
      </div>
      <div className="answers">
        {answers.map((a, ix) => (
          <button
            key={a.name_eng}
            className="btn"
            onClick={handleSelectAnswer}
            data-index={ix}
          >
            {ix + 1}. {a.name}
          </button>
        ))}
      </div>

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        .question {
          display: grid;
          grid-template-columns: 1fr;
          box-sizing: content-box;
        }

        .question-flag {
          height: 36vmin;
          margin: 0.4rem auto;
          box-shadow: 0 0.6rem 1rem rgba(0, 0, 0, 0.15);
        }

        .answers {
          text-align: center;
        }

        .answers .btn {
          display: inline-block;
          margin: 0.6rem 0.2rem 0.6rem;
          padding: 0.4rem 0.6rem;
          font-size: 1.2rem;
          color: #444;
          background-color: aquamarine;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }
        .answers .btn:hover {
          color: white;
          background-color: blue;
        }
      `}</style>
    </div>
  );
};
