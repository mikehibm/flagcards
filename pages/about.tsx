/* eslint-disable @typescript-eslint/no-explicit-any */
import { NextPage } from 'next';
import Link from 'next/link';
import { MyHead } from '../components/myhead';
import { Header } from '../components/header';
import { Footer } from '../components/footer';
import { useSendScreenView } from '../utils/use-send-screen-view';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

type Props = {
  isServer: boolean;
  isProduction: boolean;
};

const AboutPage: NextPage<Props> = ({ isServer, isProduction }) => {
  useSendScreenView('about');

  return (
    <>
      <MyHead>
        <title>Flags - About</title>
      </MyHead>
      <Header isServer={isServer} isProduction={isProduction} />

      <main>
        <h1>このアプリについて</h1>
        <h2>世界の国旗をおぼえるためのアプリです。</h2>
        <p>世界194カ国の国旗を収録しています。</p>
        <p>
          「おぼえる」モードと「クイズ」モードがあり、それぞれ20のステージに分かれています。
          各ステージには10カ国（最後のステージのみ4カ国）が含まれます。
        </p>

        <h3>「おぼえる」モード</h3>
        <p>
          国旗の画像をタップすると国名が表示されます。
          国旗と国名をおぼえたら緑色のチェックマークをタップしてONにして下さい。
          完全におぼえるまで何回でもくり返せます。
        </p>
        <p>ステージの一覧で進捗状況を確認することが出来ます。</p>

        <h3>「クイズ」モード</h3>
        <p>
          国名から国旗をえらぶ問題と、国旗を見て国名をえらぶ問題の2パターンがあり、ランダムにどちらかが表示されます。
        </p>
        <p>ステージ内の全ての問題に正解出来たら、そのステージはクリアです。</p>

        <h2>さあ、全ステージをクリアをしてあなたも国旗博士になろう！</h2>

        <div className="buttons">
          <Link href="/">
            <button className="btn-home" type="button">
              <FontAwesomeIcon icon={faArrowLeft} />
            </button>
          </Link>
        </div>
      </main>

      <Footer isServer={isServer} isProduction={isProduction} />

      <style jsx>{`
        main h1 {
          color: blue;
        }

        main h2 {
          color: darkmagenta;
        }

        .buttons {
          margin-top: 1rem;
          text-align: center;
          display: grid;
          grid-auto-columns: auto;
          grid-auto-flow: column;
          justify-content: flex-start;
          column-gap: 1.2rem;
        }
        .buttons > button {
          padding: 0.6rem 1.4rem;
          font-size: 1.5rem;
          color: #00d;
          background-color: #cdf;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }
      `}</style>
    </>
  );
};

AboutPage.getInitialProps = async () => {
  const isProduction = process.env.NODE_ENV === 'production';
  const isServer = !process.browser;
  return { isServer, isProduction };
};

export default AboutPage;
