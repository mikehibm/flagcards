import { NextPage } from 'next';
import { useCallback, useReducer, useEffect } from 'react';
import {
  learnReducer,
  ActionCreators,
  initialState,
} from '../state/learn-reducer';
const {
  selectStage,
  backToSelectStage,
  flipCard,
  checkCard,
  prevCard,
  nextCard,
  setUserData,
} = ActionCreators;
import {
  loadUserData,
  incrementLearnCount,
  clearLearnCount,
} from '../state/user-data';
import { MyHead } from '../components/myhead';
import { Header } from '../components/header';
import { Footer } from '../components/footer';
import { sendGAEvent } from '../utils/send-ga-event';
import { useSendScreenView } from '../utils/use-send-screen-view';
import { useIsTouchEnabled } from '../utils/use-is-touch-enabled';
// import { CountryList } from '../components/learn-country-list';
import { CountryCard } from '../components/learn-country-card';
import { StageList } from '../components/stage-list';
import { Country } from '../state/country';

type Props = {
  isServer: boolean;
  isProduction: boolean;
};

const Page: NextPage<Props> = ({ isServer, isProduction }) => {
  useSendScreenView('learn');
  const isTouchEnabled = useIsTouchEnabled();

  const [state, dispatch] = useReducer(learnReducer, initialState);

  // Storageから非同期でUserDataを読み込む。
  useEffect(() => {
    (async () => {
      const userData = await loadUserData();
      dispatch(setUserData(userData));
    })();
  }, []);

  const handleClickStage = useCallback((e: React.MouseEvent) => {
    const stageIndex = parseInt(
      e.currentTarget.getAttribute('data-index') || '',
    );
    sendGAEvent('learn_select_stage', { stageIndex });
    dispatch(selectStage(stageIndex));
  }, []);

  const nextStage = useCallback((stageIndex: number) => {
    sendGAEvent('learn_button_next', { stageIndex });
    dispatch(selectStage(stageIndex));
  }, []);

  const handleBackToStages = useCallback(() => {
    dispatch(backToSelectStage());
  }, []);

  const handleFlipCard = useCallback(
    (country: Country) => {
      dispatch(flipCard(country));
    },
    [state.stages],
  );

  const handleCheckCard = useCallback(
    async (country: Country) => {
      let newData;
      if (country.learn_count > 0) {
        newData = await clearLearnCount(state.userData, country.name_eng);
      } else {
        newData = await incrementLearnCount(state.userData, country.name_eng);
      }

      dispatch(checkCard(country, newData));
    },
    [state.userData, state.stages],
  );

  const handlePrevCard = useCallback(() => {
    dispatch(prevCard());
  }, [state.stages, state.stageIndex, state.cardIndex]);

  const handleNextCard = useCallback(() => {
    dispatch(nextCard());
  }, [state.stages, state.stageIndex, state.cardIndex]);

  return (
    <>
      <MyHead>
        <title>Flags - Learn</title>
      </MyHead>
      <Header isServer={isServer} isProduction={isProduction} />

      <main>
        <nav className="nav" />
        {state.page === 'stages' && (
          <StageList
            stages={state.stages}
            mode="learn"
            onClickStage={handleClickStage}
            isTouchEnabled={isTouchEnabled}
          />
        )}
        {state.page === 'learn' && (
          <CountryCard
            stage={state.stages[state.stageIndex]}
            cardIndex={state.cardIndex}
            onPrevCard={handlePrevCard}
            onNextCard={handleNextCard}
            onBackToStages={handleBackToStages}
            onFlipCard={handleFlipCard}
            onCheckCard={handleCheckCard}
          />
          // <CountryList
          //     stageIndex={state.stageIndex}
          //     stage={state.stages[state.stageIndex]}
          //     onBackToStages={handleBackToStages}
          //     onNextStage={nextStage}
          //     onFlipCard={handleFlipCard}
          //     onCheckCard={handleCheckCard}
          //     isTouchEnabled={isTouchEnabled}
          //   />
        )}
      </main>

      <Footer isServer={isServer} isProduction={isProduction} />
    </>
  );
};

Page.getInitialProps = async () => {
  const isProduction = process.env.NODE_ENV === 'production';
  const isServer = !process.browser;
  return { isServer, isProduction };
};

export default Page;
