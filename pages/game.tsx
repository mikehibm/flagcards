import { NextPage } from 'next';
import { useCallback, useReducer, useEffect, useState } from 'react';
import {
  gameReducer,
  ActionCreators,
  initialState,
} from '../state/game-reducer';
const {
  selectStage,
  backToSelectStage,
  selectAnswer,
  nextQuestion,
  setUserData,
} = ActionCreators;
import {
  loadUserData,
  incrementGameCount,
  decrementGameCount,
} from '../state/user-data';
import { MyHead } from '../components/myhead';
import { Header } from '../components/header';
import { Footer } from '../components/footer';
import { sendGAEvent } from '../utils/send-ga-event';
import { useSendScreenView } from '../utils/use-send-screen-view';
import { useIsTouchEnabled } from '../utils/use-is-touch-enabled';
import { Quiz } from '../components/quiz';
import { StageList } from '../components/stage-list';

type Props = {
  isServer: boolean;
  isProduction: boolean;
};

const Page: NextPage<Props> = ({ isServer, isProduction }) => {
  useSendScreenView('game');
  const isTouchEnabled = useIsTouchEnabled();

  const [state, dispatch] = useReducer(gameReducer, initialState);

  const [isQuestionResultHidden, setIsQuestionResultHidden] = useState(false);

  // Storageから非同期でUserDataを読み込む。
  useEffect(() => {
    (async () => {
      const userData = await loadUserData();
      dispatch(setUserData(userData));
    })();
  }, []);

  const handleClickStage = useCallback((e: React.MouseEvent) => {
    const stageIndex = parseInt(
      e.currentTarget.getAttribute('data-index') || '',
    );
    sendGAEvent('game_select_stage', { stageIndex });
    dispatch(selectStage(stageIndex));
  }, []);

  const handleNextQuestion = useCallback(() => {
    sendGAEvent('game_button_next');
    if (
      state.questionIndex >=
      state.stages[state.stageIndex].countries.length - 1
    ) {
      dispatch(backToSelectStage());
    } else {
      dispatch(nextQuestion());
    }
  }, [state.questionIndex, state.stageIndex, state.stages]);

  const handleBackToStages = useCallback(() => {
    dispatch(backToSelectStage());
  }, []);

  const handleSelectAnswer = async (answerIndex: number) => {
    const question =
      state.stages[state.stageIndex].countries[state.questionIndex];
    const isCorrect =
      question.name_eng === question.answers[answerIndex].name_eng;

    let newData = state.userData;
    if (isCorrect) {
      if (state.questionResult === null) {
        newData = await incrementGameCount(state.userData, question.name_eng);
      }
    } else {
      newData = await decrementGameCount(state.userData, question.name_eng);
    }
    dispatch(selectAnswer(answerIndex, newData));

    // 正解・不正解のアイコンを表示。
    setIsQuestionResultHidden(false);
    if (isCorrect) {
      // 正解なら自動的に次の問題へ。
      setTimeout(handleNextQuestion, 500);
    } else {
      setTimeout(() => {
        setIsQuestionResultHidden(true);
      }, 800);
    }
  };

  return (
    <>
      <MyHead>
        <title>Flags - Quiz</title>
      </MyHead>
      <Header isServer={isServer} isProduction={isProduction} />

      <main>
        <nav className="nav" />
        {state.page === 'stages' && (
          <StageList
            stages={state.stages}
            mode="game"
            onClickStage={handleClickStage}
            isTouchEnabled={isTouchEnabled}
          />
        )}
        {state.page === 'game' && (
          <Quiz
            stage={state.stages[state.stageIndex]}
            questionIndex={state.questionIndex}
            questionResult={state.questionResult}
            isQuestionResultHidden={isQuestionResultHidden}
            onNextQuestion={handleNextQuestion}
            onSelectAnswer={handleSelectAnswer}
            onBackToStages={handleBackToStages}
          />
        )}
      </main>

      <Footer isServer={isServer} isProduction={isProduction} />
    </>
  );
};

Page.getInitialProps = async () => {
  const isProduction = process.env.NODE_ENV === 'production';
  const isServer = !process.browser;
  return { isServer, isProduction };
};

export default Page;
