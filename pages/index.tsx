/* eslint-disable @typescript-eslint/no-explicit-any */
import { NextPage } from 'next';
import Link from 'next/link';
import { MyHead } from '../components/myhead';
import { Header } from '../components/header';
import { Footer } from '../components/footer';
import { useSendScreenView } from '../utils/use-send-screen-view';

type Props = {
  isServer: boolean;
  isProduction: boolean;
};

const Page: NextPage<Props> = ({ isServer, isProduction }) => {
  useSendScreenView('home');

  return (
    <>
      <MyHead>
        <title>Flags</title>
      </MyHead>
      <Header isServer={isServer} isProduction={isProduction} />

      <main>
        <Link href="/learn">
          <button>おぼえる</button>
        </Link>
        <Link href="/game">
          <button>クイズ</button>
        </Link>
        <Link href="/about">
          <button className="btn-about">せつめい</button>
        </Link>
        <Link href="/preference">
          <button className="btn-pref">設定</button>
        </Link>
      </main>

      <Footer isServer={isServer} isProduction={isProduction} />

      {/* eslint-disable-next-line prettier/prettier */}
      <style jsx>{`
        main {
          margin-top: 1.2rem;
          text-align: center;
        }
        button {
          margin: 1rem;
          min-width: 12rem;
          min-height: 3.4rem;
          font-size: 1.4rem;
          color: blue;
          background-color: rgb(231, 239, 255);
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.5rem;
          cursor: pointer;
        }
        button[disabled] {
          color: gray;
          cursor: default;
        }

        button.btn-about,
        button.btn-pref {
          /*color: rgb(241, 54, 126);*/
          background-color: rgb(255, 247, 221);
        }

        a {
          text-decoration: none;
        }

        @media (max-aspect-ratio: 10/10) {
          main {
            margin-top: 1.8rem;
          }
        }
      `}</style>
    </>
  );
};

Page.getInitialProps = async () => {
  const isProduction = process.env.NODE_ENV === 'production';
  const isServer = !process.browser;
  return { isServer, isProduction };
};

export default Page;
