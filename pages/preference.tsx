/* eslint-disable @typescript-eslint/no-explicit-any */
import { useCallback, useState, useEffect } from 'react';
import { NextPage } from 'next';
import Link from 'next/link';
import { MyHead } from '../components/myhead';
import { Header } from '../components/header';
import { Footer } from '../components/footer';
import { AlertDialog } from '../components/alert-dialog';
import { ConfirmDialog } from '../components/confirm-dialog';
import { useSendScreenView } from '../utils/use-send-screen-view';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import {
  resetAllCounts,
  loadUserData,
  hasAnyProgress,
} from '../state/user-data';

type Props = {
  isServer: boolean;
  isProduction: boolean;
};

const PreferencePage: NextPage<Props> = ({ isServer, isProduction }) => {
  useSendScreenView('preference');

  const [isResetConfirmDialogOpen, setIsResetConfirmDialogOpen] = useState(
    false,
  );

  const [resetDone, setResetDone] = useState(false);

  const [resetEnabled, setResetEnabled] = useState(false);
  useEffect(() => {
    (async () => {
      const userData = await loadUserData();
      setResetEnabled(hasAnyProgress(userData));
    })();
  }, [isResetConfirmDialogOpen]);

  const handleClickResetProgress = useCallback(() => {
    setIsResetConfirmDialogOpen(true);
  }, []);

  const handleResetProgress = useCallback(async () => {
    setIsResetConfirmDialogOpen(false);
    await resetAllCounts();
    setResetDone(true);
    console.log('resetAllCounts DONE.');
  }, []);

  const handleCancelResetProgress = useCallback(() => {
    console.log('handleCancelResetProgress');
    setIsResetConfirmDialogOpen(false);
  }, []);

  return (
    <>
      <MyHead>
        <title>Flags - Preference</title>
      </MyHead>
      <Header isServer={isServer} isProduction={isProduction} />

      <main>
        <h1>設定</h1>

        <section>
          <h2>進捗</h2>
          {!resetEnabled && !resetDone && (
            <div className="reset-result">
              まだ進捗状況のデータはありません。毎日少しずつおぼえましょう！
            </div>
          )}
          {(resetEnabled || resetDone) && (
            <>
              <button
                className="btn-reset"
                onClick={handleClickResetProgress}
                disabled={resetDone || isResetConfirmDialogOpen}
              >
                最初からやりなおす
              </button>
            </>
          )}
          {isResetConfirmDialogOpen && (
            <ConfirmDialog
              message="進捗状況のデータを消去しますか？"
              onOkCallback={handleResetProgress}
              onNgCallback={handleCancelResetProgress}
            />
          )}
          {resetDone && (
            <AlertDialog message="進捗状況のデータを消去しました。もう一度はじめからトライしましょう！" />
          )}
        </section>

        {/* <section>
          <h2>言語</h2>
          <button className="btn-lang ja current">日本語 - ja</button>
          <button className="btn-lang en">英語 - en</button>
        </section> */}

        <div className="buttons">
          <Link href="/">
            <button className="btn-home" type="button">
              <FontAwesomeIcon icon={faArrowLeft} />
            </button>
          </Link>
        </div>
      </main>

      <Footer isServer={isServer} isProduction={isProduction} />

      <style jsx>{`
        main h1 {
          color: rgb(31, 31, 160);
        }

        main section {
          margin-bottom: 2rem;
        }

        main section h2 {
          color: darkmagenta;
          padding-left: 1rem;
        }

        main section button {
          display: block;
          width: 70vw;
          min-width: 10rem;
          max-width: 20rem;
          margin: 0 auto;
          padding: 0.6rem 1.4rem;

          font-size: 1.1rem;
          color: #00d;
          background-color: rgb(213, 227, 255);
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }
        main section button[disabled] {
          color: gray;
          cursor: default;
        }

        main section button.btn-lang.current {
          color: red;
          border: 0.2rem solid rgba(253, 106, 106, 0.4);
        }

        .reset-result {
          margin: 1rem;
          padding: 1rem;
          color: rgb(36, 35, 136);
          background-color: rgb(236, 235, 235);
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
        }

        .buttons {
          margin-top: 3rem;
          text-align: center;
          display: grid;
          grid-auto-columns: auto;
          grid-auto-flow: column;
          justify-content: flex-start;
          column-gap: 1.2rem;
        }
        .buttons > button {
          padding: 0.6rem 1.4rem;
          font-size: 1.5rem;
          color: #00d;
          background-color: #cdf;
          border: 0.2rem solid rgba(80, 80, 80, 0.4);
          border-radius: 0.6rem;
          cursor: pointer;
        }
      `}</style>
    </>
  );
};

PreferencePage.getInitialProps = async () => {
  const isProduction = process.env.NODE_ENV === 'production';
  const isServer = !process.browser;
  return { isServer, isProduction };
};

export default PreferencePage;
