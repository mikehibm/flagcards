import { useState, useEffect } from 'react';

export function useIsTouchEnabled() {
  // 端末がタッチ対応かどうかを検出。
  const [isTouchEnabled, setIsTouchEnabled] = useState(false);
  useEffect(() => {
    const hasTouchStartEvent = 'ontouchstart' in document.documentElement;
    setIsTouchEnabled(hasTouchStartEvent);
  }, []);
  return isTouchEnabled;
}
