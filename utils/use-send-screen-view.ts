import { useEffect } from 'react';
import { sendGAEvent } from './send-ga-event';

export function useSendScreenView(screenName: string) {
  return useEffect(() => {
    console.log(`useSendScreenView: ${screenName}`);
    const params = { screen_name: screenName };
    sendGAEvent('screen_view', params);
  }, []);
}
