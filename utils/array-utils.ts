/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */

// 配列をシャッフル
//
// 参考： https://qiita.com/amanomunt/items/7bd686f58b9cc59d9d07
export function shuffle<T>(arr: Array<T>): Array<T> {
  for (let i = arr.length - 1; i > 0; i--) {
    // i = ランダムに選ぶ終点のインデックス
    const j = Math.floor(Math.random() * (i + 1)); // j = 範囲内から選ぶランダム変数
    [arr[j], arr[i]] = [arr[i], arr[j]]; // 分割代入 i と j を入れ替える
  }
  return arr;
}

// 連番の配列を生成
export function createNumbers(length: number) {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return Array.from({ length }, (_: any, ix: number) => ix);
}
