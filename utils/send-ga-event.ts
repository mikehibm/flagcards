/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-undef */

export function sendGAEvent(eventName: string, params?: object) {
  if (!window) return;
  const firebase = (window as any).firebase;
  if (!firebase) return;
  console.log(`Sending GA event '${eventName}, ${JSON.stringify(params)}'.`);
  try {
    firebase.analytics().logEvent(eventName, params);
  } catch (error) {
    console.error(error);
  }
}
