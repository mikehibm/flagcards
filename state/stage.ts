import { Country, getCountries } from './country';
import { UserData } from './user-data';
import { STAGE_COUNT } from '../utils/constants';
import { createNumbers, shuffle } from '../utils/array-utils';

export type Stage = {
  stageIndex: number;
  countries: Country[];
  allDone: boolean;
};

function getRandomCountries(
  src: Country[],
  count: number,
  include: Country,
): Country[] {
  if (src.length < count) count = src.length;
  // console.log(`getRandomCountries: `, src, count, include);
  const excluded = src.filter(i => i !== include);
  const list: Country[] = excluded.slice(0, count - 1);
  list.push(include);
  return shuffle(list);
}

export function getStages(
  userData: UserData,
  mode: 'learn' | 'game',
  gameMode: 'flagToName' | 'nameToFlag' | 'mixed',
): Stage[] {
  return createNumbers(STAGE_COUNT).map(i => {
    const countries = getCountries(i, userData, gameMode);
    return {
      stageIndex: i,
      countries: countries.map(c => ({
        ...c,
        answers: getRandomCountries(countries, 5, c),
      })),
      allDone: countries.every(c =>
        mode === 'learn' ? c.learn_count > 0 : c.game_count > 0,
      ),
    };
  });
}

export function getStageProgress(stage: Stage, mode: 'learn' | 'game'): number {
  const countDone = stage.countries.reduce((prev, cur) => {
    return (
      prev +
      (mode === 'learn'
        ? cur.learn_count > 0
          ? 1
          : 0
        : cur.game_count > 0
        ? 1
        : 0)
    );
  }, 0);
  const progress = countDone / stage.countries.length;
  return progress;
}
