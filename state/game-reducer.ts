import { Stage, getStages } from './stage';
import { UserData } from './user-data';

export type GameState = {
  page: 'stages' | 'game';
  gameMode: 'flagToName' | 'nameToFlag' | 'mixed';
  stages: Stage[];
  stageIndex: number;
  questionIndex: number;
  questionResult: 'OK' | 'NG' | null;
  userData: UserData;
};

export const initialState: GameState = {
  page: 'stages',
  gameMode: 'mixed',
  stages: [] as Stage[],
  stageIndex: 0,
  questionIndex: 0,
  questionResult: null,
  userData: {} as UserData,
};

const types = {
  BACK_TO_SELECT_STAGE: 'BACK_TO_SELECT_STAGE',
  SELECT_STAGE: 'SELECT_STAGE',
  SELECT_ANSWER: 'SELECT_ANSWER',
  NEXT_QUESTION: 'NEXT_QUESTION',
  SET_USER_DATA: 'SET_USER_DATA',
} as const;

export const ActionCreators = {
  backToSelectStage: () => ({
    type: types.BACK_TO_SELECT_STAGE,
  }),

  selectStage: (stageIndex: number) => ({
    type: types.SELECT_STAGE,
    payload: { stageIndex },
  }),

  selectAnswer: (answerIndex: number, userData: UserData) => ({
    type: types.SELECT_ANSWER,
    payload: { answerIndex, userData },
  }),

  nextQuestion: () => ({
    type: types.NEXT_QUESTION,
  }),

  setUserData: (userData: UserData) => ({
    type: types.SET_USER_DATA,
    payload: { userData },
  }),
};

export type GameAction =
  | ReturnType<typeof ActionCreators.backToSelectStage>
  | ReturnType<typeof ActionCreators.selectStage>
  | ReturnType<typeof ActionCreators.selectAnswer>
  | ReturnType<typeof ActionCreators.nextQuestion>
  | ReturnType<typeof ActionCreators.setUserData>;

function getQuestionResult(state: GameState, answerIndex: number): 'OK' | 'NG' {
  const question =
    state.stages[state.stageIndex].countries[state.questionIndex];
  return question.name_eng === question.answers[answerIndex].name_eng
    ? 'OK'
    : 'NG';
}

function handlerSelectAnswer(state: GameState, answerIndex: number): Stage[] {
  const stage = state.stages[state.stageIndex];
  const prevResult = state.questionResult;

  const countries = stage.countries.map(c => ({
    ...c,
    game_count:
      c === stage.countries[state.questionIndex]
        ? c.name_eng === c.answers[answerIndex].name_eng
          ? prevResult === null
            ? c.game_count + 1
            : c.game_count
          : Math.max(c.game_count - 1, 0)
        : c.game_count,
  }));

  const allDone = countries.every(c => c.game_count > 0);
  const newStage = {
    ...stage,
    countries,
    allDone,
  };

  state.stages.splice(state.stageIndex, 1, newStage);
  return [...state.stages];
}

export function gameReducer(state: GameState, action: GameAction): GameState {
  switch (action.type) {
    case types.BACK_TO_SELECT_STAGE:
      return {
        ...state,
        page: 'stages',
        stages: getStages(state.userData, 'game', state.gameMode),
        stageIndex: 0,
        questionIndex: 0,
        questionResult: null,
      };

    case types.SELECT_STAGE:
      return {
        ...state,
        page: 'game',
        stages: getStages(state.userData, 'game', state.gameMode),
        stageIndex: action.payload.stageIndex,
        questionIndex: 0,
        questionResult: null,
      };

    case types.SELECT_ANSWER:
      return {
        ...state,
        stages: handlerSelectAnswer(state, action.payload.answerIndex),
        questionResult: getQuestionResult(state, action.payload.answerIndex),
        userData: action.payload.userData,
      };

    case types.NEXT_QUESTION:
      return {
        ...state,
        questionIndex:
          state.questionIndex <
          state.stages[state.stageIndex].countries.length - 1
            ? state.questionIndex + 1
            : 0,
        questionResult: null,
        stageIndex:
          state.questionIndex <
          state.stages[state.stageIndex].countries.length - 1
            ? state.stageIndex
            : 0,
        page:
          state.questionIndex <
          state.stages[state.stageIndex].countries.length - 1
            ? state.page
            : 'stages',
      };

    case types.SET_USER_DATA:
      return {
        ...state,
        stages: getStages(action.payload.userData, 'game', state.gameMode),
        userData: action.payload.userData,
      };
  }
}
