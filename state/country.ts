import { UserData } from './user-data';
import { STAGE_COUNT } from '../utils/constants';
import { shuffle } from '../utils/array-utils';
import { countries as allCountries } from '../country-data.json';

export type Country = {
  name: string;
  name_eng: string;
  flag_ratio: string;
  flag_url: string;
  flag_file: string;
  flipped: boolean;

  learn_count: number;
  game_count: number;
  gameMode: 'flagToName' | 'nameToFlag';
  answers: Country[];
};

function mergeUserData(countries: Country[], userData: UserData): Country[] {
  return countries.map(c => ({
    ...c,
    learn_count:
      userData && userData.countries[c.name_eng]
        ? userData.countries[c.name_eng].learn_count
        : 0,
    game_count:
      userData && userData.countries[c.name_eng]
        ? userData.countries[c.name_eng].game_count
        : 0,
  }));
}

function randomGameMode(): 'nameToFlag' | 'flagToName' {
  return Math.floor(Math.random() * 100) >= 50 ? 'nameToFlag' : 'flagToName';
}

export function getCountries(
  stageIndex: number,
  userData: UserData,
  gameMode: 'flagToName' | 'nameToFlag' | 'mixed',
): Country[] {
  // console.log(`getCountries: stageIndex=${stageIndex}`);
  if (stageIndex >= STAGE_COUNT) stageIndex = STAGE_COUNT - 1;

  const flagCount = Math.ceil(allCountries.length / STAGE_COUNT);
  const startIndex = flagCount * stageIndex;
  const endIndex = startIndex + flagCount;
  const countries: Country[] = allCountries
    .slice(startIndex, endIndex)
    .map(i => {
      return {
        ...i,
        flipped: false,
        learn_count: 0,
        game_count: 0,
        gameMode: gameMode === 'mixed' ? randomGameMode() : gameMode,
        answers: [] as Country[],
      };
    });
  const newCountries = shuffle(countries) as Country[];
  return mergeUserData(newCountries, userData);
}
