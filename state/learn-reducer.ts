import { Country } from './country';
import { Stage, getStages } from './stage';
import { UserData } from './user-data';

export type LearnState = {
  page: 'stages' | 'learn';
  stages: Stage[];
  stageIndex: number;
  cardIndex: number;
  userData: UserData;
};

export const initialState: LearnState = {
  page: 'stages',
  stages: [] as Stage[],
  stageIndex: 0,
  cardIndex: 0,
  userData: {} as UserData,
};

const types = {
  BACK_TO_SELECT_STAGE: 'BACK_TO_SELECT_STAGE',
  SELECT_STAGE: 'SELECT_STAGE',
  FLIP_CARD: 'FLIP_CARD',
  CHECK_CARD: 'CHECK_CARD',
  PREV_CARD: 'PREV_CARD',
  NEXT_CARD: 'NEXT_CARD',
  SET_USER_DATA: 'SET_USER_DATA',
} as const;

export const ActionCreators = {
  backToSelectStage: () => ({
    type: types.BACK_TO_SELECT_STAGE,
  }),

  selectStage: (stageIndex: number) => ({
    type: types.SELECT_STAGE,
    payload: { stageIndex },
  }),

  flipCard: (country: Country) => ({
    type: types.FLIP_CARD,
    payload: { country },
  }),

  checkCard: (country: Country, userData: UserData) => ({
    type: types.CHECK_CARD,
    payload: { country, userData },
  }),

  prevCard: () => ({
    type: types.PREV_CARD,
  }),

  nextCard: () => ({
    type: types.NEXT_CARD,
  }),

  setUserData: (userData: UserData) => ({
    type: types.SET_USER_DATA,
    payload: { userData },
  }),
};

export type LearnAction =
  | ReturnType<typeof ActionCreators.backToSelectStage>
  | ReturnType<typeof ActionCreators.selectStage>
  | ReturnType<typeof ActionCreators.flipCard>
  | ReturnType<typeof ActionCreators.checkCard>
  | ReturnType<typeof ActionCreators.prevCard>
  | ReturnType<typeof ActionCreators.nextCard>
  | ReturnType<typeof ActionCreators.setUserData>;

function setFlippedAll(state: LearnState, flipped: boolean): Stage[] {
  const stage = state.stages[state.stageIndex];
  const countries = stage.countries.map(i => {
    return { ...i, flipped: flipped };
  });
  state.stages.splice(state.stageIndex, 1, {
    ...stage,
    countries,
  });
  return [...state.stages];
}

// function setFlippedCurrentCard(state: LearnState, flipped: boolean): Stage[] {
//   const stage = state.stages[state.stageIndex];
//   const country = state.stages[state.stageIndex].countries[state.cardIndex];
//   const countries = stage.countries.map(i => {
//     return i === country ? { ...i, flipped: flipped } : { ...i };
//   });
//   state.stages.splice(state.stageIndex, 1, {
//     ...stage,
//     countries,
//   });
//   return [...state.stages];
// }

function flipOneCard(state: LearnState, country: Country): Stage[] {
  const stage = state.stages[state.stageIndex];
  const countries = stage.countries.map(i => {
    return i === country ? { ...i, flipped: !i.flipped } : { ...i };
  });
  state.stages.splice(state.stageIndex, 1, {
    ...stage,
    countries,
  });
  return [...state.stages];
}

function checkOneCard(state: LearnState, country: Country): Stage[] {
  const stage = state.stages[state.stageIndex];
  const countries = stage.countries.map(i => {
    return i === country
      ? { ...i, learn_count: i.learn_count === 0 ? 1 : 0 }
      : { ...i };
  });

  const allDone = countries.every(c => c.learn_count > 0);
  const newStage = {
    ...stage,
    countries,
    allDone,
  };

  state.stages.splice(state.stageIndex, 1, newStage);
  return [...state.stages];
}

export function learnReducer(
  state: LearnState,
  action: LearnAction,
): LearnState {
  switch (action.type) {
    case types.BACK_TO_SELECT_STAGE:
      return {
        ...state,
        page: 'stages',
        stages: getStages(state.userData, 'learn', 'mixed'),
        stageIndex: 0,
        cardIndex: 0,
      };

    case types.SELECT_STAGE:
      return {
        ...state,
        page: 'learn',
        stages: getStages(state.userData, 'learn', 'mixed'),
        stageIndex: action.payload.stageIndex,
        cardIndex: 0,
      };

    case types.FLIP_CARD:
      return {
        ...state,
        stages: flipOneCard(state, action.payload.country),
      };

    case types.CHECK_CARD:
      return {
        ...state,
        stages: checkOneCard(state, action.payload.country),
        userData: action.payload.userData,
      };

    case types.PREV_CARD:
      return {
        ...state,
        cardIndex: state.cardIndex > 0 ? state.cardIndex - 1 : 0,
        stages: setFlippedAll(state, false), //setFlippedCurrentCard(state, false),
      };

    case types.NEXT_CARD:
      return {
        ...state,
        cardIndex:
          state.cardIndex < state.stages[state.stageIndex].countries.length - 1
            ? state.cardIndex + 1
            : state.cardIndex,
        stages: setFlippedAll(state, false), //setFlippedCurrentCard(state, false),
      };

    case types.SET_USER_DATA:
      return {
        ...state,
        stages: getStages(action.payload.userData, 'learn', 'mixed'),
        userData: action.payload.userData,
      };
  }
}
