export type UserDataCountryItem = {
  learn_count: number;
  game_count: number;
};

export type UserData = {
  countries: {
    [name_eng: string]: UserDataCountryItem;
  };
};

const KEY_USERDATA = 'USERDATA';
const DEFAULT_VALUE = {
  countries: {},
} as UserData;

const DEFAULT_VALUE_COUNTRY: UserDataCountryItem = {
  learn_count: 0,
  game_count: 0,
};

export function updateCountry(
  data: UserData,
  name_eng: string,
  country: UserDataCountryItem,
) {
  return {
    ...data,
    countries: {
      ...data.countries,
      [name_eng]: country,
    },
  };
}

export async function loadUserData(): Promise<UserData> {
  if (!window || !window.localStorage) return DEFAULT_VALUE;
  console.log('loadUserData');
  return new Promise((resolve, reject) => {
    try {
      const s = localStorage.getItem(KEY_USERDATA);
      const data = s ? (JSON.parse(s) as UserData) : DEFAULT_VALUE;
      resolve(data);
    } catch (error) {
      reject(error);
    }
  });
}

export async function saveUserData(data: UserData): Promise<void> {
  if (!window || !window.localStorage) return;
  console.log('saveUserData');
  return new Promise((resolve, reject) => {
    try {
      localStorage.setItem(KEY_USERDATA, JSON.stringify(data));
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

type FieldNames = keyof UserDataCountryItem;

async function clearCount(
  data: UserData,
  name_eng: string,
  field_name: FieldNames,
) {
  const country = data.countries[name_eng] || DEFAULT_VALUE_COUNTRY;

  const updated = {
    ...country,
    [field_name]: 0,
  };

  const newData = updateCountry(data, name_eng, updated);
  await saveUserData(newData);
  return newData;
}

async function addCount(
  data: UserData,
  name_eng: string,
  field_name: FieldNames,
  diff: number,
) {
  console.log('addCount', name_eng, field_name, diff);

  const country = data.countries[name_eng] || DEFAULT_VALUE_COUNTRY;
  const newValue = Math.max(
    0,
    country[field_name] ? country[field_name] + diff : diff,
  );

  const updated = {
    ...country,
    [field_name]: newValue,
  };

  console.log('updated', updated);

  const newData = updateCountry(data, name_eng, updated);

  await saveUserData(newData);
  return newData;
}

export async function incrementLearnCount(
  data: UserData,
  name_eng: string,
): Promise<UserData> {
  return await addCount(data, name_eng, 'learn_count', 1);
}

export async function decrementLearnCount(
  data: UserData,
  name_eng: string,
): Promise<UserData> {
  return await addCount(data, name_eng, 'learn_count', -1);
}

export async function clearLearnCount(
  data: UserData,
  name_eng: string,
): Promise<UserData> {
  return await clearCount(data, name_eng, 'learn_count');
}

export async function incrementGameCount(
  data: UserData,
  name_eng: string,
): Promise<UserData> {
  return await addCount(data, name_eng, 'game_count', 1);
}

export async function decrementGameCount(
  data: UserData,
  name_eng: string,
): Promise<UserData> {
  return await addCount(data, name_eng, 'game_count', -1);
}

export async function clearGameCount(
  data: UserData,
  name_eng: string,
): Promise<UserData> {
  return await clearCount(data, name_eng, 'game_count');
}

export async function resetAllCounts(): Promise<void> {
  const data = await loadUserData();

  const newData = { ...data };
  for (const name_eng in newData.countries) {
    newData.countries[name_eng] = {
      ...newData.countries[name_eng],
      learn_count: 0,
      game_count: 0,
    };
  }

  await saveUserData(newData);
}

export function hasAnyProgress(data: UserData): boolean {
  for (const name_eng in data.countries) {
    const country = data.countries[name_eng];
    if (country.learn_count > 0 || country.game_count > 0) {
      return true;
    }
  }
  return false;
}
