/* eslint-disable no-undef */

if (firebase) {
  console.log('Sending GA page_view.');
  firebase.analytics().logEvent('page_view');
}
